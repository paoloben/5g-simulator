# 5G-simulator
### _an open source framework to simulate LTE and 5G networks_
---
##### Table of Contents:
1. Getting 5G-simulator
2. Compiling 5G-simulator
3. Running 5G-simulator
---
##### 1. Getting 5G-simulator
5G-simulator is available via Git at https://bitbucket.org/telematicslab/5g-simulator
To obtain 5G-simulator enter into the your prefered folder and write the following syntax:
   
    $ git clone https://bitbucket.org/telematicslab/5g-simulator.git
To synchronize the project repository with the local copy, you can run the pull sub-command. The syntax is as follows:
    
    $ git pull
##### 2. Compiling 5G-simulator
On recent Linux systems, you can build 5G-simulator with the following command:

	$ make
To clear the project, you can use the following command:

	$ make clean
##### 3. Running 5G-simulator
In this release several scenarios have been developed. To run a simple simulation, you can use the following command:

	$ ./5G-simulator Simple 

For more details about available scenarios, use

	$ ./5G-simulator -h
---
### _Giuseppe Piro_
###### _Software manager and main developer_
(c) 2018

TELEMATICS LAB - Politecnico di Bari

g.piro@poliba.it

peppe@giuseppepiro.com
