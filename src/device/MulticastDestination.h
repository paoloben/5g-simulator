/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2018 TELEMATICS LAB, Politecnico di Bari
 *
 * This file is part of 5G-simulator
 *
 * 5G-simulator is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation;
 *
 * 5G-simulator is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with 5G-simulator; if not, see <http://www.gnu.org/licenses/>.
 *
 * Author: Alessandro Grassi <alessandro.grassi@poliba.it>
 */



#ifndef MULTICASTDESTINATION_H_
#define MULTICASTDESTINATION_H_

#include "UserEquipment.h"

class ENodeB;
class Gateway;
class CqiManager;


class MulticastDestination : public UserEquipment
{
public:
  MulticastDestination () = default;
  MulticastDestination (int idElement,
                        Cell *cell,
                        NetworkNode* target);

  virtual ~MulticastDestination();

  void MakeActive (void);

  bool IsIndoor (void);

  //Debug
  void Print (void);

  void AddSource(NetworkNode *n);
  void DeleteSource(NetworkNode *n);
  vector<NetworkNode*> GetSources();
  void SetSources(vector<NetworkNode*> sources);

  void AddDestination(NetworkNode *n);
  void DeleteDestination(NetworkNode *n);
  vector<NetworkNode*> GetDestinations();
  void SetDestinations(vector<NetworkNode*> destinations);

  void SetMcs(int m_mcs);
  int GetMcs();

private:
  int m_mcs;
  vector<NetworkNode*> m_sources;
  vector<NetworkNode*> m_destinations;

};

#endif /* MULTICASTDESTINATION_H_ */
