/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2018 TELEMATICS LAB, Politecnico di Bari
 *
 * This file is part of 5G-simulator
 *
 * 5G-simulator is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation;
 *
 * 5G-simulator is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with 5G-simulator; if not, see <http://www.gnu.org/licenses/>.
 *
 * Author: Giuseppe Piro <g.piro@poliba.it>
 */


#include <stdint.h>
#include <list>
#include "packet-burst.h"


PacketBurst::~PacketBurst (void)
{
  for (list<Packet* >::const_iterator iter = m_packets.begin (); iter
       != m_packets.end (); ++iter)
    {
      delete *iter;
    }
  m_packets.clear();
}

shared_ptr<PacketBurst>
PacketBurst::Copy (void)
{
  shared_ptr<PacketBurst> pb = make_shared<PacketBurst> ();

  list<Packet* > packets = GetPackets ();
  list<Packet* >::iterator it;

  for (it = packets.begin (); it != packets.end (); it++)
    {
      Packet* packet = (*it)->Copy();
      pb->AddPacket (packet);
    }

  return pb;
}

void
PacketBurst::AddPacket (Packet* packet)
{
  if (packet)
    {
      m_packets.push_back (packet);
    }
}

list<Packet*>
PacketBurst::GetPackets (void) const
{
  return m_packets;
}

uint32_t
PacketBurst::GetNPackets (void) const
{
  return m_packets.size ();
}

uint32_t
PacketBurst::GetSize (void) const
{
  uint32_t size = 0;
  for (list<Packet* >::const_iterator iter = m_packets.begin (); iter
       != m_packets.end (); ++iter)
    {
      Packet* packet = *iter;
      size += packet->GetSize ();
    }
  return size;
}

list<Packet* >::const_iterator
PacketBurst::Begin (void) const
{
  return m_packets.begin ();
}

list<Packet* >::const_iterator
PacketBurst::End (void) const
{
  return m_packets.end ();
}



