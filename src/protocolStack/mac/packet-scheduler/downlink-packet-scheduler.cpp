/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2018 TELEMATICS LAB, Politecnico di Bari
 *
 * This file is part of 5G-simulator
 *
 * 5G-simulator is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation;
 *
 * 5G-simulator is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with 5G-simulator; if not, see <http://www.gnu.org/licenses/>.
 *
 * Author: Giuseppe Piro <g.piro@poliba.it>
 *         Lukasz Rajewski <lukasz.rajewski@gmail.com> (optimized PRB allocation)
 */

#include <algorithm>
#include <memory>
#include <armadillo>

#include "downlink-packet-scheduler.h"
#include "../mac-entity.h"
#include "../harq-manager.h"
#include "../../packet/Packet.h"
#include "../../packet/packet-burst.h"
#include "../../../device/NetworkNode.h"
#include "../../../device/MulticastDestination.h"
#include "../../../flows/radio-bearer.h"
#include "../../../protocolStack/rrc/rrc-entity.h"
#include "../../../flows/application/Application.h"
#include "../../../flows/application/ExternalSource.h"
#include "../../../flows/application/TraceBased.h"
#include "../../../flows/QoS/QoSParameters.h"
#include "../../../device/ENodeB.h"
#include "../../../device/UserEquipment.h"
#include "../../../protocolStack/mac/AMCModule.h"
#include "../../../phy/lte-phy.h"
#include "../../../phy/enb-lte-phy.h"
#include "../../../phy/sinr-calculator.h"
#include "../../../core/spectrum/bandwidth-manager.h"
#include "../../../flows/MacQueue.h"
#include "../../../utility/eesm-effective-sinr.h"
#include "../../../utility/miesm-effective-sinr.h"
#include "../../../componentManagers/FrameManager.h"

DownlinkPacketScheduler::DownlinkPacketScheduler()
{
  m_maxUsersPerRB = 1;
}

DownlinkPacketScheduler::~DownlinkPacketScheduler()
{
  Destroy ();
}

void
DownlinkPacketScheduler::SetMaxUsersPerRB (int users)
{
  m_maxUsersPerRB = users;
}

int
DownlinkPacketScheduler::GetMaxUsersPerRB (void)
{
  return m_maxUsersPerRB;
}


void DownlinkPacketScheduler::SelectFlowsToSchedule ()
{
DEBUG_LOG_START_1(LTE_SIM_SCHEDULER_DEBUG)
  cout << "\t Select Flows to schedule" << endl;
DEBUG_LOG_END

  RrcEntity *rrc = GetMacEntity ()->GetDevice ()->GetProtocolStack ()->GetRrcEntity ();
  for (auto bearer : *rrc->GetRadioBearerContainer ())
    {
      //SELECT FLOWS TO SCHEDULE
      if (bearer->HasPackets ()
          &&
          bearer->GetDestination ()->GetNodeState () == NetworkNode::STATE_ACTIVE
          &&
          (
            FrameManager::Init()->MbsfnEnabled()==false
            ||
            (
              FrameManager::Init()->MbsfnEnabled()==true
              &&
              FrameManager::Init()->isMbsfnSubframe()==true
              &&
              bearer->GetDestination()->GetNodeType()==NetworkNode::TYPE_MULTICAST_DESTINATION
            )
            ||
            (
              FrameManager::Init()->MbsfnEnabled()==true
              &&
              FrameManager::Init()->isMbsfnSubframe()==false
              &&
              bearer->GetDestination()->GetNodeType()!=NetworkNode::TYPE_MULTICAST_DESTINATION
            )
          )
         )
        {
          //compute data to transmit
          int dataToTransmit;
          if (bearer->GetApplication ()->GetApplicationType () == Application::APPLICATION_TYPE_INFINITE_BUFFER)
            {
              dataToTransmit = 100000000;
            }
          else
            {
              dataToTransmit = bearer->GetQueueSize ();
            }

          //compute spectral efficiency
          ENodeB *enb = (ENodeB*) GetMacEntity ()->GetDevice ();
          UserEquipment *ue = (UserEquipment*) bearer->GetDestination ();
          ENodeB::UserEquipmentRecord *ueRecord = ue->GetTargetNodeRecord ();
          if (ueRecord->CqiAvailable()==false)
            {
              continue;
            }
          AMCModule* amc = ueRecord->GetUE()->GetProtocolStack()->GetMacEntity()->GetAmcModule();
          vector<double> spectralEfficiency;
          vector<int> cqiFeedbacks = ueRecord->GetCQI ();
          int riFeedback = ueRecord->GetRI ();
          vector< vector<int> > pmiFeedbacks = ueRecord->GetPMI ();
          vector< shared_ptr<arma::cx_fmat> > fullCsiFeedbacks = ueRecord->GetChannelMatrix ();
          int numberOfCqi = cqiFeedbacks.size ();
          for (int i = 0; i < numberOfCqi; i++)
            {
              double sEff = amc->GetEfficiencyFromCQI (cqiFeedbacks.at (i));
              spectralEfficiency.push_back (sEff);
            }

          //create flow to schedule record
          InsertFlowToSchedule(bearer, dataToTransmit, spectralEfficiency, cqiFeedbacks, riFeedback, pmiFeedbacks, fullCsiFeedbacks);
        }
      else
        {}
    }
}

void
DownlinkPacketScheduler::DoSchedule (void)
{
DEBUG_LOG_START_1(LTE_SIM_SCHEDULER_DEBUG)
  cout << "Start DL packet scheduler for node "
            << GetMacEntity ()->GetDevice ()->GetIDNetworkNode()<< endl;
DEBUG_LOG_END

  UpdateAverageTransmissionRate ();
  SelectFlowsToSchedule ();

  if (GetFlowsToSchedule ()->size() == 0)
    {}
  else
    {
      RBsAllocation ();
    }

  StopSchedule ();
  ClearFlowsToSchedule ();
}

void
DownlinkPacketScheduler::DoStopSchedule (void)
{
DEBUG_LOG_START_1(LTE_SIM_SCHEDULER_DEBUG)
  cout << "\t Creating Packet Burst" << endl;
DEBUG_LOG_END

  shared_ptr<PacketBurst> pb = make_shared<PacketBurst> ();

  //Create Packet Burst
  for (auto flow : *GetFlowsToSchedule ())
    {
      int availableBytes = flow->GetAllocatedBits ()/8;

      if (availableBytes > 0)
        {

          flow->GetBearer ()->UpdateTransmittedBytes (availableBytes);

DEBUG_LOG_START_1(LTE_SIM_SCHEDULER_DEBUG)
          cout << "\t  --> add packets for flow "
                    << flow->GetBearer ()->GetApplication ()->GetApplicationID () << endl;
DEBUG_LOG_END
          shared_ptr<PacketBurst> pb2;
          if (flow->IsHarqRetransmission () == false)
            {
              RlcEntity *rlc = flow->GetBearer ()->GetRlcEntity ();
              pb2 = rlc->TransmissionProcedure (availableBytes);
              flow->SetPacketBurst(pb2);
              if(flow->GetBearer()->GetDestination()->GetNodeType()==NetworkNode::TYPE_MULTICAST_DESTINATION)
                {
                  for (auto flowCopy : *(flow->GetMulticastClonedFlows()) )
                    {
                      flowCopy->SetPacketBurst(pb2->Copy());
                    }
                }
            }
          else
            {
              pb2 = flow->GetPacketBurst();
            }

DEBUG_LOG_START_1(LTE_SIM_SCHEDULER_DEBUG)
          cout << "\t\t  nb of packets: " << pb2->GetNPackets () << endl;
DEBUG_LOG_END

          if (pb2->GetNPackets () > 0)
            {
              for (auto packet : pb2->GetPackets ())
                {
DEBUG_LOG_START_1(LTE_SIM_SCHEDULER_DEBUG)
                  cout << "\t\t  added packet of bytes " << packet->GetSize () << endl;
                  //packet->Print ();
DEBUG_LOG_END
                  pb->AddPacket (packet->Copy ());
                }
            }
        }
    }

  //UpdateAverageTransmissionRate ();

  //SEND PACKET BURST

DEBUG_LOG_START_1(LTE_SIM_SCHEDULER_DEBUG)
  if (pb->GetNPackets () == 0)
    cout << "\t Send only reference symbols" << endl;
DEBUG_LOG_END

  GetMacEntity ()->GetDevice ()->SendPacketBurst (pb);
}



void
DownlinkPacketScheduler::RBsAllocation ()
{
DEBUG_LOG_START_1(LTE_SIM_SCHEDULER_DEBUG)
  cout << " ---- DownlinkPacketScheduler::RBsAllocation";
DEBUG_LOG_END

  // collect flows for possible HARQ retransmission
  FlowsToSchedule flowsForRetransmission;
  ENodeB::UserEquipmentRecords* ueRecords = ((ENodeB*)(GetMacEntity ()->GetDevice()))->GetUserEquipmentRecords();
  for (auto ue : *ueRecords)
    {
      HarqManager* harqManager = ue->GetHarqManager();
      if (harqManager != nullptr)
        {
          FlowsToSchedule harqFlows = harqManager->GetFlowsForRetransmission ();
          // TODO: group retransmissions to the same UE;
          for (auto flow : harqFlows)
            {
              InsertFlowForRetransmission (flow);
            }
          flowsForRetransmission.insert(flowsForRetransmission.end(),harqFlows.begin(),harqFlows.end());
        }
    }


  FlowsToSchedule* flows = GetFlowsToSchedule ();
  int nbOfRBs = GetMacEntity ()->GetDevice ()->GetPhy ()->GetBandwidthManager ()->GetDlSubChannels ().size ();

  //create a matrix of flow metrics
  double metrics[nbOfRBs][flows->size ()];
  for (int i = 0; i < nbOfRBs; i++)
    {
      for (int j = 0; j < (int)flows->size (); j++)
        {
          int harqPid;
          ENodeB::UserEquipmentRecord* ueRecord = flows->at (j)->GetUe()->GetTargetNodeRecord();
          HarqManager* harq = ueRecord->GetHarqManager ();
          if (harq != nullptr)
            {
              harqPid = harq->GetPidForNewTx ();
            }
          else
            {
              harqPid = HarqManager::HARQ_NOT_USED;
            }
          flows->at(j)->SetHarqPid (harqPid);
          if (harqPid == HarqManager::HARQ_NO_PID_AVAILABLE && flows->at(j)->IsHarqRetransmission() == false)
            {
              // no free HARQ process, flow can't be scheduled
              metrics[i][j] = -INFINITY;
            }
          else
            {
              metrics[i][j] = ComputeSchedulingMetric (flows->at (j)->GetBearer (),
                              flows->at (j)->GetSpectralEfficiency ().at (i),
                              i);
            }
        }
    }


  //create a matrix of flow metrics for HARQ flows
  double HARQ_metrics[nbOfRBs][flowsForRetransmission.size()];
  for (int i = 0; i < nbOfRBs; i++)
    {
      for (int j = 0; j < (int)flowsForRetransmission.size (); j++)
        {
           HARQ_metrics[i][j] = ComputeSchedulingMetric (flowsForRetransmission.at (j)->GetBearer (),
                  flowsForRetransmission.at (j)->GetSpectralEfficiency ().at (i),
                  i);
        }
    }


DEBUG_LOG_START_1(LTE_SIM_SCHEDULER_DEBUG)
  cout << ", available RBs " << nbOfRBs << ", flows " << flows->size () << endl;
  for (int ii = 0; ii < (int)flows->size (); ii++)
    {
      cout << "\t metrics for flow "
                << flows->at (ii)->GetBearer ()->GetApplication ()->GetApplicationID () << ":";
      for (int jj = 0; jj < nbOfRBs; jj++)
        {
          cout << " " << metrics[jj][ii];
        }
      cout << endl;
    }
DEBUG_LOG_END


  double l_dAllocatedRBCounter = 0;

  int l_iNumberOfUsers = ((ENodeB*)this->GetMacEntity()->GetDevice())->GetNbOfUserEquipmentRecords();

  bool * l_bFlowScheduled = new bool[flows->size ()];
  int l_iScheduledFlows = 0;
  vector<double> * l_bFlowScheduledSINR = new vector<double>[flows->size ()];
  for (int k = 0; k < (int)flows->size (); k++)
    l_bFlowScheduled[k] = false;

  vector<double> flowWidebandSinrs;
  for (auto f : *flows)
    {
      vector<double> sinrs;
      AMCModule* amc = f->GetUe()->GetProtocolStack()->GetMacEntity()->GetAmcModule();
      for (int rb = 0; rb < nbOfRBs; rb++)
        {
          sinrs.push_back( amc->GetSinrFromCQI (f->GetCqiFeedbacks ().at (rb)) );
        }
      flowWidebandSinrs.push_back( GetMiesmEffectiveSinr(sinrs) );
      //amc->GetSinrFromCQI (scheduledFlow->GetCqiFeedbacks ().at (rb))
    }


  vector< shared_ptr<arma::cx_fmat> > precodingMatrices;

  // vector to track how many users are allocated on each RB
  vector<int> allocationsPerRB(nbOfRBs);
  fill( allocationsPerRB.begin(), allocationsPerRB.end(), 0);

  // schedule HARQ retransmissions first
  // Every HARQ retransmission requires the same number of RBs and the same MCS as the original transmission
  FlowsToSchedule flowsForRetransmission_copy = flowsForRetransmission;
  vector<int> HarqScheduledUEs;
  for (auto harqFlow : flowsForRetransmission)
    {
      vector<int> localSchedulingMetrics(nbOfRBs);
      int flowIndex = -1;
      for (int i=0; i<(int)flowsForRetransmission.size(); i++)
        {
          if (harqFlow == flowsForRetransmission.at(i))
            {
              flowIndex = i;
            }
        }
      for (int rb = 0; rb < nbOfRBs; rb++)
        {
          localSchedulingMetrics.at(rb) = HARQ_metrics[rb][flowIndex]; // popolare HARQ_metrics
        }
      vector<int> schedulingMetricsIndices(nbOfRBs);
      iota(schedulingMetricsIndices.begin(), schedulingMetricsIndices.end(), 0);

      sort(schedulingMetricsIndices.begin(),
           schedulingMetricsIndices.end(),
           [&localSchedulingMetrics](int i1, int i2) {return localSchedulingMetrics[i1] < localSchedulingMetrics[i2];});

      int availableRBs = 0;
      for (int i = 0; i < nbOfRBs; i++)
        {
          if (allocationsPerRB.at(i) < GetMaxUsersPerRB())
            {
              availableRBs++;
            }
        }

      vector<int>* allocatedRBs = harqFlow->GetListOfAllocatedRBs();
      int numRequiredRBs = allocatedRBs->size();
      allocatedRBs->clear();


      // set MIMO layers when using full CSI feedback
      vector< shared_ptr<arma::cx_fmat> > channelMatrices;
      vector<int> assignedLayers;
      if (harqFlow->GetFullCsiFeedbacks ().size()>0)
        {
          channelMatrices.push_back(harqFlow->GetFullCsiFeedbacks ().at (0));
          for(
            int j = 0;
            j < (int)harqFlow->GetFullCsiFeedbacks ().at (0)->n_rows;
            j++
          )
            {
              assignedLayers.push_back (j);
            }
          harqFlow->SetAssignedLayers(assignedLayers);
        }


      if (availableRBs >= numRequiredRBs)
        {
          HarqScheduledUEs.push_back(flowsForRetransmission.at(flowIndex)->GetUe()->GetIDNetworkNode());
          int assignedRBs = 0;
          for (int i = 0; i < nbOfRBs; i++)
            {
              if (allocationsPerRB.at( schedulingMetricsIndices.at(i) ) < GetMaxUsersPerRB())
                {
                  allocatedRBs->push_back( schedulingMetricsIndices.at(i) );
                  allocationsPerRB.at( schedulingMetricsIndices.at(i) )++;
                  assignedRBs++;
                }
              if (assignedRBs >= numRequiredRBs)
                {
                  break;
                }
            }
        }
      else
        {
          flowsForRetransmission_copy.erase( remove( flowsForRetransmission_copy.begin(), flowsForRetransmission_copy.end(), harqFlow ) );
        }
    }
  flowsForRetransmission = flowsForRetransmission_copy;


  //RBs allocation
  for (int rb = 0; rb < nbOfRBs; rb++)
    {
      if (l_iScheduledFlows == (int)flows->size ())
        break;

      bool RBIsAllocated = false;
      if ( allocationsPerRB.at(rb) > 0 )
        {
          RBIsAllocated = true;
        }
      vector< shared_ptr<FlowToSchedule> > scheduledFlows;
      vector<int> l_iScheduledFlowsIndices;

      // schedule non-retransmission flows
      double targetMetric;
      for (int i = allocationsPerRB.at(rb); i < m_maxUsersPerRB; i++)
        {
          targetMetric = 0;
          shared_ptr<FlowToSchedule> scheduledFlow;
          int l_iScheduledFlowIndex;
          for (int k = 0; k < (int)flows->size (); k++)
            {
              if (metrics[rb][k] > targetMetric
                  && metrics[rb][k] > -INFINITY
                  && flows->at(k)->GetUe()->GetTargetNodeRecord()->GetHarqManager()->GetPidForNewTx() >= 0
                  && !l_bFlowScheduled[k]
                  && flows->at(k)->IsHarqRetransmission()==false
                  && find(HarqScheduledUEs.begin(), HarqScheduledUEs.end(), flows->at (k)->GetUe()->GetIDNetworkNode()) == HarqScheduledUEs.end()
              )
                {
                  targetMetric = metrics[rb][k];
                  RBIsAllocated = true;
                  scheduledFlow = flows->at (k);
                  l_iScheduledFlowIndex = k;
                }
            }
          if (targetMetric > 0)
            {
              allocationsPerRB.at(rb)++;
              scheduledFlows.push_back (scheduledFlow);
              l_iScheduledFlowsIndices.push_back (l_iScheduledFlowIndex);
            }
        }
DEBUG_LOG_START_1(LTE_SIM_SCHEDULER_DEBUG)
      cout << "Number of flows on RB " << rb << ": " << scheduledFlows.size() << endl;
DEBUG_LOG_END

      // detect which flows have fullfilled their requirements
      if (RBIsAllocated)
        {
          l_dAllocatedRBCounter++;
          for (int i = 0; i < (int)scheduledFlows.size(); i++)
            {
              shared_ptr<FlowToSchedule> scheduledFlow = scheduledFlows.at(i);
              AMCModule* amc = scheduledFlow->GetUe()->GetProtocolStack()->GetMacEntity()->GetAmcModule();
              scheduledFlow->GetListOfAllocatedRBs()->push_back (rb); // the rb RB has been allocated to that flow!
              int flowIndex = distance (flows->begin (),find (flows->begin (),flows->end (),scheduledFlow));

DEBUG_LOG_START_1(LTE_SIM_SCHEDULER_DEBUG)
              cout << "\t *** RB " << rb << " assigned to the "
                        " flow " << scheduledFlow->GetBearer ()->GetApplication ()->GetApplicationID ()
                        << " (UE " << scheduledFlow->GetUe()->GetIDNetworkNode()
                        << " rank " << scheduledFlow->GetRiFeedback() << ")"
                        << endl;
DEBUG_LOG_END
              double sinr = amc->GetSinrFromCQI (scheduledFlow->GetCqiFeedbacks ().at (rb));
              l_bFlowScheduledSINR[l_iScheduledFlowsIndices.at(i)].push_back(sinr);
              int mcs = amc->GetMCSFromSinrVector (l_bFlowScheduledSINR[l_iScheduledFlowsIndices.at(i)]);
              int txMode = scheduledFlow->GetUe()->GetTargetNodeRecord()->GetDlTxMode();
              int M, N, rank;
              M = scheduledFlow->GetBearer ()->GetSource ()->GetPhy()->GetTxAntennas();
              N = scheduledFlow->GetBearer ()->GetDestination ()->GetPhy()->GetRxAntennas();
              switch(txMode)
                {
                case 1:
                case 2:
                  rank = 1;
                  break;
                case 3:
                case 4:
                case 9:
                  rank = scheduledFlow->GetRiFeedback();
                  if(rank < 1 || rank > min(M,N))
                    {
                      rank = min(M,N);
                    }
                  break;
                default:
                  cout << "Error: invalid transmission mode " << txMode << endl;
                  break;
                }
              int transportBlockSize = amc->GetTBSizeFromMCS (mcs, mcs, scheduledFlow->GetListOfAllocatedRBs ()->size (),rank);
              if (transportBlockSize >= scheduledFlow->GetDataToTransmit() * 8)
                {
                  l_bFlowScheduled[l_iScheduledFlowsIndices.at(i)] = true;
                  l_iScheduledFlows++;
                }
            }
        }
    }

  delete [] l_bFlowScheduled;
  delete [] l_bFlowScheduledSINR;

  flows->insert(flows->end(),flowsForRetransmission.begin(),flowsForRetransmission.end());

DEBUG_LOG_START_1(LTE_SIM_SCHEDULER_DEBUG)
  cout << "SCHEDULING T=" << Simulator::Init()->Now() << " UEs ";
  for (auto flow : *flows)
    {
      if (flow->GetListOfAllocatedRBs ()->size () > 0)
        {
          cout << flow->GetUe()->GetIDNetworkNode() << " ";
        }
    }
  cout << endl;
DEBUG_LOG_END

  //Finalize the allocation
  PdcchMapIdealControlMessage *pdcchMsg = new PdcchMapIdealControlMessage ();

  for (auto flow : *flows)
    {
      ENodeB::UserEquipmentRecord* ueRecord = flow->GetUe()->GetTargetNodeRecord();
      int txMode = ueRecord->GetDlTxMode();
      int M = flow->GetBearer ()->GetSource ()->GetPhy()->GetTxAntennas();
      int N = flow->GetBearer ()->GetDestination ()->GetPhy()->GetRxAntennas();
      int mcs, rank;
      AMCModule* amc = flow->GetUe()->GetProtocolStack()->GetMacEntity()->GetAmcModule();

      if (flow->GetListOfAllocatedRBs ()->size () > 0)
        {
          vector<int> pmi;

          if (flow->IsHarqRetransmission() == false)
            {
              //this flow has been scheduled
              vector<double> estimatedSinrValues;

              for (int rb = 0; rb < (int)flow->GetListOfAllocatedRBs ()->size (); rb++ )
                {
                  double sinr;
                  sinr = amc->GetSinrFromCQI (flow->GetCqiFeedbacks ().at (flow->GetListOfAllocatedRBs ()->at (rb)));
                  estimatedSinrValues.push_back (sinr);
                }

              //get the MCS for transmission

              if (FrameManager::Init()->isMbsfnSubframe()==true)
                {
                  MulticastDestination* mc = (MulticastDestination*)flow->GetBearer()->GetApplication()->GetDestination();
                  int mc_mcs = mc->GetMcs();
                  if(mc_mcs == -1)
                    {
                      mcs = amc->GetMCSFromSinrVector (estimatedSinrValues);
                    }
                  else if(mc_mcs>=0 && mc_mcs<=28)
                    {
                      mcs = mc_mcs;
                    }
                  else
                    {
                      cout << "Wrong MCS set for multicast stream: " << mc_mcs << endl;
                      exit(1);
                    }

//                  int burstSize = ((TraceBased*)(flow->GetBearer()->GetApplication()))->GetBurstSize();
//                  double maxDelay = flow->GetBearer()->GetApplication()->GetQoSParameters()->GetMaxDelay();
//                  int minTbs;
//                  switch(FrameManager::Init()->GetMbsfnPattern())
//                    {
//                      case 1:
//                        minTbs = round( burstSize / (maxDelay * 1000 * 1. / 10.) * 8 );
//                        break;
//                      case 2:
//                        minTbs = round( burstSize / (maxDelay * 1000 * 3. / 10.) * 8 );
//                        break;
//                      case 3:
//                        minTbs = round( burstSize / (maxDelay * 1000 * 6. / 10.) * 8 );
//                        break;
//                      default:
//                        minTbs = round( burstSize / (maxDelay * 1000 * 1. / 10.) * 8 );
//                        break;
//                    }
//
//                  AMCModule* amc = new AMCModule();
//                  for(mcs=0; mcs<27; mcs++)
//                    {
//                      if (amc->GetTBSizeFromMCS (mcs, nbOfRBs)>minTbs)
//                        {
//                          break;
//                        }
//                    }
//                  delete amc;
                }
              else
                {
                  mcs = amc->GetMCSFromSinrVector (estimatedSinrValues);
                }
              for (int rb = 0; rb < (int)flow->GetListOfAllocatedRBs ()->size (); rb++ )
                {
                  flow->GetListOfSelectedMCS()->push_back(mcs);
                }
            }

          rank = flow->GetRiFeedback();
          switch(txMode)
            {
            case 1:
            case 2:
              rank = 1;
              pmi.resize(0); // not used
              break;

            case 3:
              if(rank < 1 || rank > min(M,N))
                {
                  cout << "Error: invalid rank in DownlinkPacketScheduler (" << rank << ")" << endl;
                  rank = min(M,N);
                }
              pmi.resize(0); //not used
DEBUG_LOG_START_1(LTE_SIM_TEST_RI_FEEDBACK)
              cout << "flow "
                        << flow->GetBearer ()->GetApplication ()->GetApplicationID ()
                        << ",\tRI feedback: "
                        << flow->GetRiFeedback()
                        << endl;
DEBUG_LOG_END
              break;

            case 4:
            case 9:
              if(rank < 1 || rank > min(M,N))
                {
                  cout << "Error: invalid rank in DownlinkPacketScheduler (" << rank << ")" << endl;
                  rank = min(M,N);
                }
              break;
            }

          if (flow->IsHarqRetransmission() == false)
            {
              //define the amount of bytes to transmit
              int transportBlockSize = amc->GetTBSizeFromMCS (mcs, mcs, flow->GetListOfAllocatedRBs ()->size (), rank );
              flow->UpdateAllocatedBits (transportBlockSize);

DEBUG_LOG_START_1(LTE_SIM_SCHEDULER_DEBUG)
              cout << "\t\t --> flow " << flow->GetBearer ()->GetApplication ()->GetApplicationID ()
                        << " has been scheduled: " <<
                        "\n\t\t\t nb of RBs " << flow->GetListOfAllocatedRBs ()->size () <<
//                      "\n\t\t\t effectiveSinr " << GetMiesmEffectiveSinr(estimatedSinrValues) <<
                        "\n\t\t\t tbs " << transportBlockSize
                        << endl;
DEBUG_LOG_END
DEBUG_LOG_START_1(LTE_SIM_DL_DEBUG)
              cout << "DL_DEBUG "
                        << flow->GetBearer ()->GetApplication ()-> GetDestination ()-> GetMobilityModel ()->GetAbsolutePosition ()->GetCoordinateX () << " "
                        << flow->GetBearer ()->GetApplication ()-> GetDestination ()-> GetMobilityModel ()->GetAbsolutePosition ()->GetCoordinateY () << " "
//                << GetMiesmEffectiveSinr(estimatedSinrValues) << " "
                        << mcs << " "
                        << rank << " "
                        << transportBlockSize
                        << endl;
DEBUG_LOG_END
            }

          //create PDCCH messages
          vector<int> assignedLayers;
          for(int i = 0; i < rank; i++)
            {
              assignedLayers.push_back (i);
            }
          for (int rb = 0; rb < (int)flow->GetListOfAllocatedRBs ()->size (); rb++ )
            {
              if (flow->GetPmiFeedbacks ().size() > 0)
                {
                  pmi = flow->GetPmiFeedbacks ().at (flow->GetListOfAllocatedRBs ()->at (rb));
                }
              else
                {
                  pmi.resize(0);
                }
              if (pmi.size() == 0)
                {
                  pmi.resize(1);
                  if (M == 2 && rank == 2)
                    {
                      pmi.at(0) = 1;
                    }
                  else
                    {
                      pmi.at(0) = 0;
                    }
                  if(txMode == 9 && M == 8)
                    {
                      pmi.resize(2);
                      pmi.at(1) = 0;
                    }
                }
              if (flow->GetBearer ()->GetDestination ()->GetNodeType() == NetworkNode::TYPE_MULTICAST_DESTINATION)
                {
                  // schedule this RB to each user of the multicast group
                  MulticastDestination* virtualDest = (MulticastDestination*)flow->GetBearer ()->GetDestination ();
                  for(auto dest : virtualDest->GetDestinations())
                    {
                      ENodeB::UserEquipmentRecord* userRecord = ((UserEquipment*)dest)->GetTargetNodeRecord();
                      if (userRecord !=nullptr)
                        {
                          HarqManager* harq = userRecord->GetHarqManager ();
                          int harqPID;
                          if (harq != nullptr)
                            {
                              harqPID = userRecord->GetHarqManager ()->GetPidForNewTx ();
                            }
                          else
                            {
                              harqPID = HarqManager::HARQ_NOT_USED;
                            }
                          pdcchMsg->AddNewRecord (PdcchMapIdealControlMessage::DOWNLINK, flow->GetListOfAllocatedRBs ()->at (rb), dest, mcs, harqPID, rank, pmi);
                        }
                    }
                }
              else
                {
                  pdcchMsg->AddNewRecord (PdcchMapIdealControlMessage::DOWNLINK,
                                          flow->GetListOfAllocatedRBs ()->at (rb),
                                          flow->GetBearer ()->GetDestination (),
                                          flow->GetListOfSelectedMCS ()->at (rb),
                                          flow->GetHarqPid(), rank, pmi, assignedLayers);
                }
            }

          // notify operation to HARQ manager

          if(flow-> GetBearer ()->GetDestination ()->GetNodeType() == NetworkNode::TYPE_MULTICAST_DESTINATION)
            {
              MulticastDestination* virtualDest = (MulticastDestination*)flow->GetBearer ()->GetDestination ();
              for(auto dest : virtualDest->GetDestinations())
                {
                  ENodeB::UserEquipmentRecord* userRecord = ((UserEquipment*)dest)->GetTargetNodeRecord();
                  if (userRecord != nullptr)
                    {
                      HarqManager* harqman = userRecord->GetHarqManager ();
                      int harqPID;
                      if (harqman != nullptr)
                        {
                          harqPID = userRecord->GetHarqManager ()->GetPidForNewTx ();
                          /*}
                          else
                          {
                            harqPID = HarqManager::HARQ_NOT_USED;
                          } */
//                          RadioBearer* rbearer;
//                          int data;
                          shared_ptr<FlowToSchedule> flowcp = make_shared<FlowToSchedule>(
                              flow->GetBearer(),
                              flow->GetDataToTransmit());
                          *flowcp = *flow;
                          flowcp ->SetHarqPid (harqPID);
                          harqman->NewMulticastTransmission (harqPID,flowcp);
                          flow->GetMulticastClonedFlows()->push_back(flowcp);
                        }
                    }
                }
            }
          else if (flow->IsHarqRetransmission() == true)
            {
              // increase retransmission counter
              ueRecord->GetHarqManager ()->Retransmission (flow->GetHarqPid());
            }
          else if (flow->GetHarqPid() != HarqManager::HARQ_NOT_USED)
            {
              // copy flow to HARQ manager
              ueRecord->GetHarqManager ()->NewTransmission (flow->GetHarqPid(), flow);
            }
        }
    }

  if (pdcchMsg->GetMessage()->size () > 0)
    {
      GetMacEntity ()->GetDevice ()->GetPhy ()->SendIdealControlMessage (pdcchMsg);
    }
}


void
DownlinkPacketScheduler::UpdateAverageTransmissionRate (void)
{
  RrcEntity *rrc = GetMacEntity ()->GetDevice ()->GetProtocolStack ()->GetRrcEntity ();
  for (auto bearer : *rrc->GetRadioBearerContainer ())
    {
      bearer->UpdateAverageTransmissionRate ();
    }
}
